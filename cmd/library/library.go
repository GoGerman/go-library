package library

import (
	"gitlab.com/GoGerman/go-library/internal/database"
	"gitlab.com/GoGerman/go-library/internal/models"
	"gitlab.com/GoGerman/go-library/internal/modules/handlers"
	"gitlab.com/GoGerman/go-library/internal/modules/repository"
	"gitlab.com/GoGerman/go-library/internal/modules/service"
	"gitlab.com/GoGerman/go-library/internal/router"
	"log"
)

func RunLibrary() {
	conf := models.DB{
		Host:     "postgres",
		Port:     "5432",
		Password: "1234",
		User:     "user",
		Name:     "postgres",
		Driver:   "postgres",
	}
	db, err := database.CreateDB(conf)
	if err != nil {
		log.Fatalln(err)
	}
	repo := repository.NewLibraryRepository(db)
	serv := service.NewLibraryService(repo)
	hand := handlers.NewLibraryHandle(serv)
	router.RunServer(hand)

	query := "DROP TABLE IF EXISTS public.authors"
	_, _ = db.Exec(query)
	query = "DROP TABLE IF EXISTS public.books"
	_, _ = db.Exec(query)
	query = "DROP TABLE IF EXISTS public.users"
	_, _ = db.Exec(query)
}
