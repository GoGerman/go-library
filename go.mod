module gitlab.com/GoGerman/go-library

go 1.20

require (
	github.com/brianvoe/gofakeit/v6 v6.21.0
	github.com/go-chi/chi v1.5.4
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.9
)
