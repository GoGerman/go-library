package service

import (
	"gitlab.com/GoGerman/go-library/internal/models"
)

type LibInterface interface {
	CreateAuthor(author models.Author) (models.Author, error)
	CreateBook(book models.Book) (models.Book, error)
	GetAuthors() ([]models.Author, error)
	GetBooks() ([]models.Book, error)
	GetUsersRentedBook() ([]models.User, error)
	RentBook(book models.RentBook) error
	ReturnBook(bookReturn models.RentBook) error
	FakeGenerate() (string, error)
	GetAuthorsTop() ([]models.Author, error)
	CreateUser(user models.User) (models.User, error)
}

type Service struct {
	serv LibInterface
}

func NewLibraryService(repo LibInterface) LibInterface {
	return &Service{
		serv: repo,
	}
}

func (s *Service) CreateAuthor(author models.Author) (models.Author, error) {
	return s.serv.CreateAuthor(author)
}

func (s *Service) CreateBook(book models.Book) (models.Book, error) {
	return s.serv.CreateBook(book)
}

func (s *Service) CreateUser(user models.User) (models.User, error) {
	return s.serv.CreateUser(user)
}

func (s *Service) GetAuthors() ([]models.Author, error) {
	return s.serv.GetAuthors()
}

func (s *Service) GetBooks() ([]models.Book, error) {
	return s.serv.GetBooks()
}

func (s *Service) GetUsersRentedBook() ([]models.User, error) {
	return s.serv.GetUsersRentedBook()
}

func (s *Service) RentBook(bookRent models.RentBook) error {
	return s.serv.RentBook(bookRent)
}

func (s *Service) ReturnBook(bookReturn models.RentBook) error {
	return s.serv.ReturnBook(bookReturn)
}

func (s *Service) FakeGenerate() (string, error) {
	return s.serv.FakeGenerate()
}

func (s *Service) GetAuthorsTop() ([]models.Author, error) {
	return s.serv.GetAuthorsTop()
}
