package repository

const (
	CreateAuthorQueries = `
	INSERT INTO public.authors (id, first_name, second_name, rent_count)
	VALUES ($1, $2, $3, $4)
	RETURNING "id", "first_name", "second_name", "rent_count"
	`

	CreateUserQueries = `
	INSERT INTO public.users (id, first_name, second_name)
	VALUES ($1, $2, $3)
	RETURNING "id", "first_name", "second_name"
	`

	CreateBookQueries = `
	INSERT INTO public.books (title, author_id, is_taken, is_taken_by)
	VALUES ($1, $2, $3, $4)
	RETURNING "id", "title", "author_id", "is_taken", "is_taken_by"
	`

	GetAuthorsQueries = `
	SELECT *
	FROM public.authors
	`

	GetTopAuthorsQueries = `
	SELECT *
	FROM public.authors
	ORDER BY rent_count DESC 
	`

	GetBooksQueries = `
	SELECT id, title, author_id, is_taken, is_taken_by
	FROM public.books
	`

	GetUsersQueries = `
	SELECT id, first_name, second_name
	FROM public.users
	`

	RentBookQueries = `
	UPDATE public.books
	SET is_taken = $1, is_taken_by = $2
	WHERE id = $3
	`

	ReturnBookQueries = `
	UPDATE public.books
	SET is_taken = $1, is_taken_by = $2
	WHERE id = $3;
	`

	GetBookQueries = `
	SELECT is_taken, is_taken_by
	FROM public.books
	WHERE id = $1
	`
)
