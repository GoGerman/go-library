package repository

import (
	"context"
	"errors"
	"github.com/jmoiron/sqlx"
	"gitlab.com/GoGerman/go-library/internal/generator"
	"gitlab.com/GoGerman/go-library/internal/models"
	"log"
)

type LibInterface interface {
	CreateAuthor(author models.Author) (models.Author, error)
	CreateBook(book models.Book) (models.Book, error)
	CreateUser(user models.User) (models.User, error)
	GetAuthors() ([]models.Author, error)
	GetBooks() ([]models.Book, error)
	GetUsersRentedBook() ([]models.User, error)
	RentBook(bookRent models.RentBook) error
	ReturnBook(bookReturn models.RentBook) error
	FakeGenerate() (string, error)
	GetAuthorsTop() ([]models.Author, error)
}

type Repository struct {
	db *sqlx.DB
}

func NewLibraryRepository(db *sqlx.DB) LibInterface {
	return &Repository{
		db: db,
	}
}

func (r *Repository) CreateAuthor(author models.Author) (models.Author, error) {
	createdAuthor := models.Author{}
	query := CreateAuthorQueries
	err := r.db.QueryRowxContext(context.TODO(), query, &author.ID, &author.SecondName,
		&author.FirstName, &author.RentCount).StructScan(&createdAuthor)
	if err != nil {
		return models.Author{}, errors.New("can't create author")
	} else {
		return createdAuthor, nil
	}
}

func (r *Repository) CreateBook(book models.Book) (models.Book, error) {
	createdBook := models.Book{}
	query := CreateBookQueries
	err := r.db.QueryRowxContext(context.TODO(), query, &book.Title, &book.AuthorID,
		&book.IsTaken, &book.IsTakenBy).StructScan(&createdBook)
	if err != nil {
		log.Println(book)
		log.Println(err)
		return models.Book{}, errors.New("can't create book")
	} else {
		return createdBook, nil
	}
}

func (r *Repository) CreateUser(user models.User) (models.User, error) {
	createdUser := models.User{}
	query := CreateUserQueries
	err := r.db.QueryRowxContext(context.TODO(), query, &user.ID, &user.FirstName,
		&user.SecondName).StructScan(&createdUser)
	if err != nil {
		log.Println(err)
		return models.User{}, errors.New("can't create book")
	} else {
		return createdUser, nil
	}
}

func (r *Repository) GetAuthors() ([]models.Author, error) {
	var authors []models.Author

	queryAuthor := GetAuthorsQueries
	queryBook := GetBooksQueries

	rowsAuthor, err := r.db.Query(queryAuthor)
	if err != nil {
		return nil, err
	}
	defer rowsAuthor.Close()

	for rowsAuthor.Next() {
		var author models.Author
		if err = rowsAuthor.Scan(
			&author.ID,
			&author.FirstName,
			&author.SecondName,
			&author.RentCount,
		); err != nil {
			return nil, err
		}

		rowsBook, err := r.db.Query(queryBook)
		if err != nil {
			return nil, err
		}
		defer rowsBook.Close()

		for rowsBook.Next() {
			var book models.Book
			if err = rowsBook.Scan(
				&book.ID,
				&book.Title,
				&book.AuthorID,
				&book.IsTaken,
				&book.IsTakenBy,
			); err != nil {
				return nil, err
			}
			if book.AuthorID == author.ID {
				author.Books = append(author.Books, book)
			}
		}
		authors = append(authors, author)
	}
	return authors, nil
}

func (r *Repository) GetBooks() ([]models.Book, error) {
	var books []models.Book
	queryBook := GetBooksQueries

	rowsBook, err := r.db.Query(queryBook)
	if err != nil {
		return nil, err
	}
	defer rowsBook.Close()

	for rowsBook.Next() {
		var book models.Book
		if err = rowsBook.Scan(
			&book.ID,
			&book.Title,
			&book.AuthorID,
			&book.IsTaken,
			&book.IsTakenBy,
		); err != nil {
			return nil, err
		}
		books = append(books, book)
	}
	return books, nil
}

func (r *Repository) GetUsersRentedBook() ([]models.User, error) {
	var users []models.User

	queryBook := GetBooksQueries
	queryUser := GetUsersQueries

	rowsUser, err := r.db.Query(queryUser)
	if err != nil {
		return nil, err
	}
	defer rowsUser.Close()

	for rowsUser.Next() {
		var user models.User
		if err = rowsUser.Scan(
			&user.ID,
			&user.FirstName,
			&user.SecondName,
		); err != nil {
			return nil, err
		}

		rowsBook, err := r.db.Query(queryBook)
		if err != nil {
			return nil, err
		}
		defer rowsBook.Close()

		for rowsBook.Next() {
			var book models.Book
			if err = rowsBook.Scan(
				&book.ID,
				&book.Title,
				&book.AuthorID,
				&book.IsTaken,
				&book.IsTakenBy,
			); err != nil {
				return nil, err
			}
			if book.IsTakenBy == user.ID {
				user.RentedBooks = append(user.RentedBooks, book)
			}
		}
		users = append(users, user)
	}
	return users, nil
}

func (r *Repository) RentBook(bookRent models.RentBook) error {

	queryBook := GetBookQueries

	var isTaken bool
	var isTakenBy int
	err := r.db.QueryRow(queryBook, bookRent.IDBook).Scan(&isTaken, &isTakenBy)
	if err != nil {
		return err
	}

	if isTaken == true {
		return errors.New("already is rent")
	}

	queryBook = RentBookQueries
	err = r.db.QueryRow(queryBook, true, bookRent.IDUser,
		bookRent.IDBook).Err()
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) ReturnBook(bookReturn models.RentBook) error {

	queryBook := GetBookQueries

	var isTaken bool
	var isTakenBy int
	err := r.db.QueryRow(queryBook, bookReturn.IDBook).Scan(&isTaken, &isTakenBy)
	if err != nil {
		return err
	}

	if isTaken == false {
		return errors.New("book is available")
	}

	queryBook = ReturnBookQueries
	err = r.db.QueryRow(queryBook, false, 0,
		bookReturn.IDBook).Err()
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) FakeGenerate() (string, error) {
	users, authors, books := generator.GenerateEntity()
	log.Println("Start generating")
	for i := range users {
		_, err := r.CreateUser(users[i])
		if err != nil {
			return "can't fake generate user", err
		}
	}
	log.Println("Generated user")
	for i := range authors {
		_, err := r.CreateAuthor(authors[i])
		if err != nil {
			return "can't fake generate authors", err
		}
	}
	log.Println("Generated authors")
	for i := range books {
		_, err := r.CreateBook(books[i])
		if err != nil {
			return "can't fake generate books", err
		}
	}
	log.Println("Generated books")
	return "success", nil
}

func (r *Repository) GetAuthorsTop() ([]models.Author, error) {
	var authors []models.Author
	queryAuthor := GetTopAuthorsQueries
	rowsAuthor, err := r.db.Query(queryAuthor)
	if err != nil {
		return nil, err
	}
	defer rowsAuthor.Close()

	var count int

	for rowsAuthor.Next() {
		var author models.Author
		if err = rowsAuthor.Scan(
			&author.ID,
			&author.FirstName,
			&author.SecondName,
			&author.RentCount,
		); err != nil {
			return nil, err
		}
		if count == 10 {
			break
		}
		authors = append(authors, author)
		count++
	}

	return authors, nil
}
