package generator

import (
	"github.com/brianvoe/gofakeit/v6"
	"gitlab.com/GoGerman/go-library/internal/models"
	"log"
	"sync"
)

var Wg sync.WaitGroup

func GenerateEntity() ([]models.User, []models.Author, []models.Book) {
	log.Println("Start generate entities")
	//var (
	//	users   []models.User
	//	authors []models.Author
	//	books   []models.Book
	//)
	//Wg.Add(3)
	//go func() {
	//	authors = generateAuthors()
	//	Wg.Done()
	//}()
	//go func() {
	//	users = generateUsers()
	//	Wg.Done()
	//}()
	//go func() {
	//	books = generateBooks()
	//	Wg.Done()
	//}()
	//Wg.Wait()
	defer log.Println("Done generate entities")
	return generateUsers(), generateAuthors(), generateBooks()
	//return users, authors, books
}

func generateUsers() []models.User {
	gofakeit.Seed(0)
	log.Println("Start generate users")
	users := make([]models.User, 100)
	for i := 0; i < len(users); i++ {
		user := models.User{
			ID:         i,
			FirstName:  gofakeit.FirstName(),
			SecondName: gofakeit.LastName(),
		}
		users[i] = user
	}
	log.Println("Done generate users")
	return users
}

func generateAuthors() []models.Author {
	gofakeit.Seed(0)
	log.Println("Start generate authors")
	authors := make([]models.Author, 100)
	for i := 0; i < len(authors); i++ {
		author := models.Author{
			ID:         i,
			FirstName:  gofakeit.FirstName(),
			SecondName: gofakeit.LastName(),
		}
		authors[i] = author
	}
	log.Println("Done generate authors")
	return authors
}

func generateBooks() []models.Book {
	gofakeit.Seed(0)
	log.Println("Start generate books")
	var books []models.Book
	var book models.Book
	for i := 0; i < 1000; i++ {
		book = models.Book{
			ID:        i,
			Title:     gofakeit.Sentence(2),
			AuthorID:  gofakeit.IntRange(0, 99),
			IsTaken:   false,
			IsTakenBy: 0,
		}
		books = append(books, book)
	}
	log.Println("Done generate books")
	return books
}
