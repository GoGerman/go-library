package models

type Author struct {
	ID         int    `db:"id" json:"id"`
	FirstName  string `db:"first_name" json:"first_name"`
	SecondName string `db:"second_name" json:"second_name"`
	RentCount  int    `db:"rent_count" json:"rent_count"`
	Books      []Book `json:"books"`
}
