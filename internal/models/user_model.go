package models

type User struct {
	ID          int    `db:"id" json:"id"`
	FirstName   string `db:"first_name" json:"first_name"`
	SecondName  string `db:"second_name" json:"second_name"`
	RentedBooks []Book `json:"rented_books"`
}
