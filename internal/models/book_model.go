package models

type Book struct {
	ID        int    `db:"id" json:"id"`
	Title     string `db:"title" json:"title"`
	AuthorID  int    `db:"author_id" json:"author_id"`
	IsTaken   bool   `db:"is_taken" json:"is_taken"`
	IsTakenBy int    `db:"is_taken_by" json:"is_taken_by"`
}

type RentBook struct {
	IDBook int `json:"id_book"`
	IDUser int `json:"id_user"`
}
