package docs

import "gitlab.com/GoGerman/go-library/internal/models"

//go:generate swagger generate spec -o ../public/swagger.json --scan-models

// swagger:route GET /books library GetBooksHandleRequest
// Получение всех books.
// responses:
//   200: GetBooksHandleResponse

// swagger:response GetBooksHandleResponse
type GetBooksHandleResponse struct {
	// in:body
	Body []models.Book
}

// swagger:route GET /authors library GetAuthorsHandleRequest
// Получение всех authors.
// responses:
//   200: GetAuthorsHandleResponse

// swagger:response GetAuthorsHandleResponse
type GetAuthorsHandleResponse struct {
	// in:body
	Body []models.Author
}

// swagger:route GET /authors/top library GetAuthorsTopHandleRequest
// Получение всех top authors.
// responses:
//   200: GetAuthorsTopHandleResponse

// swagger:response GetAuthorsTopHandleResponse
type GetAuthorsTopHandleResponse struct {
	// in:body
	Body []models.Author
}

// swagger:route GET /users/rented library GetUsersRentedBookHandleRequest
// Получение всех rented books by users.
// responses:
//   200: GetUsersRentedBookHandleResponse

// swagger:response GetUsersRentedBookHandleResponse
type GetUsersRentedBookHandleResponse struct {
	// in:body
	Body []models.User
}

// swagger:route GET /generate library FakeGenerateHandleRequest
// Создание всех fake entity.
// responses:
//   200: FakeGenerateHandleResponse

// swagger:response FakeGenerateHandleResponse
type FakeGenerateHandleResponse struct {
	// in:body
	Body string
}

// swagger:route POST /book/create library CreateBookHandleRequest
// Создание book.
// responses:
//   200: CreateBookHandleResponse

// swagger:parameters CreateBookHandleRequest
type CreateBookHandleRequest struct {
	// in:body
	Body models.Book
}

// swagger:response CreateBookHandleResponse
type CreateBookHandleResponse struct {
	// in:body
	Body models.Book
}

// swagger:route POST /book/rent library RentBookHandleRequest
// Создание book.
// responses:
//   200: RentBookHandleResponse

// swagger:parameters RentBookHandleRequest
type RentBookHandleRequest struct {
	// in:body
	Body models.RentBook
}

// swagger:response RentBookHandleResponse
type RentBookHandleResponse struct {
	// in:body
	Body string
}

// swagger:route POST /book/return library ReturnBookHandleRequest
// Возврат book.
// responses:
//   200: ReturnBookHandleResponse

// swagger:parameters ReturnBookHandleRequest
type ReturnBookHandleRequest struct {
	// in:body
	Body models.RentBook
}

// swagger:response ReturnBookHandleResponse
type ReturnBookHandleResponse struct {
	// in:body
	Body string
}

// swagger:route POST /author/create library CreateAuthorHandleRequest
// Создание author.
// responses:
//   200: CreateAuthorHandleResponse

// swagger:parameters CreateAuthorHandleRequest
type CreateAuthorHandleRequest struct {
	// in:body
	Body models.Author
}

// swagger:response CreateAuthorHandleResponse
type CreateAuthorHandleResponse struct {
	// in:body
	Body models.Author
}

// swagger:route POST /user/create library CreateUserHandleRequest
// Создание user.
// responses:
//   200: CreateUserHandleResponse

// swagger:parameters CreateUserHandleRequest
type CreateUserHandleRequest struct {
	// in:body
	Body models.User
}

// swagger:response CreateUserHandleResponse
type CreateUserHandleResponse struct {
	// in:body
	Body models.User
}
