package database

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/GoGerman/go-library/internal/models"
	"log"
)

const Create = `
	CREATE SCHEMA IF NOT EXISTS public;

	CREATE TABLE IF NOT EXISTS public.users (
    id serial primary key,
    first_name VARCHAR(255) NOT NULL,
    second_name VARCHAR(255) NOT NULL
    );
                                     
    CREATE TABLE IF NOT EXISTS public.authors (
    id serial primary key,
    first_name VARCHAR(255) NOT NULL,
    second_name VARCHAR(255) NOT NULL,
    rent_count VARCHAR(255) NOT NULL DEFAULT 0
    );
    
    CREATE TABLE IF NOT EXISTS public.books (
    id serial primary key,
    title VARCHAR(255) NOT NULL,
    author_id INTEGER NOT NULL,
    is_taken BOOLEAN NOT NULL DEFAULT false,
    is_taken_by INTEGER NOT NULL,
    FOREIGN KEY (author_id) REFERENCES authors (id),
 	FOREIGN KEY (is_taken_by) REFERENCES users (id)
    )`

func CreateDB(dbConf models.DB) (*sqlx.DB, error) {
	dbs := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		dbConf.Host, dbConf.Port, dbConf.User, dbConf.Password, dbConf.Name)
	db, err := sqlx.Open("postgres", dbs)
	if err != nil {
		log.Fatalln(err)
	}
	err = db.Ping()
	if err != nil {
		log.Fatalln(err)
	}
	_, err = db.Exec(Create)
	if err != nil {
		log.Fatalln(err)
	}

	return db, nil
}
