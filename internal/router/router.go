package router

import (
	"context"
	"fmt"
	"gitlab.com/GoGerman/go-library/internal/modules/handlers"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func RunServer(Handle *handlers.Handle) {
	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	//SwaggerUI
	r.Get("/swagger", handlers.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./internal/public"))).ServeHTTP(w, r)
	})

	//Library
	r.Get("/books", Handle.GetBooksHandle)
	r.Get("/authors", Handle.GetAuthorsHandle)
	r.Get("/authors/top", Handle.GetAuthorsTopHandle)
	r.Get("/users/rented", Handle.GetUsersRentedBookHandle)
	r.Get("/generate", Handle.FakeGenerateHandle)
	r.Post("/book/create", Handle.CreateBookHandle)
	r.Post("/book/rent", Handle.RentBookHandle)
	r.Post("/book/return", Handle.ReturnBookHandle)
	r.Post("/author/create", Handle.CreateAuthorHandle)
	r.Post("/user/create", Handle.CreateUserHandle)

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
